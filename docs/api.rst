===
API
===

.. automodule:: stukkenparser

Parsing
=======

.. autoclass:: Lexer
   :special-members: __init__, __iter__, __next__

.. autoclass:: OBLexer

.. autoclass:: WettenLexer

.. autoclass:: Parser
   :special-members: __init__

----

Onderdelen
==========

.. autoclass:: Config

.. autoclass:: Container
   :special-members: __iter__, __next__, __len__

.. autoclass:: Onderdeel

.. autoclass:: Stuk
   :show-inheritance:
   :special-members: __init__, __iter__, __next__

.. autoclass:: Kop
   :show-inheritance:

.. autoclass:: Tussenkop
   :show-inheritance:

.. autoclass:: Margetekst
   :show-inheritance:

.. autoclass:: Opsomming
   :show-inheritance:

.. autoclass:: Lijst
   :show-inheritance:

.. autoclass:: LijstItem
   :show-inheritance:

.. autoclass:: Tabel
   :show-inheritance:
   :special-members: __iter__, __next__, __getitem__

.. autoclass:: Rij
   :show-inheritance:
   :special-members: __iter__, __next__

.. autoclass:: Cel
   :show-inheritance:

.. autoclass:: Figuur
   :show-inheritance:

.. autoclass:: Alinea
   :show-inheritance:

----

.. _xml-ref:

XML
===

.. autoclass:: XMLAdapter

----

.. _rdf-ref:

RDF
===

.. autoclass:: RDFAdapter

----

Utilities
=========

.. autoclass:: TextNormaliser
