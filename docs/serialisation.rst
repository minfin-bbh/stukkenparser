
.. _serialisatie-ref:

XML
===

De stukkenparser komt standaard met een module waarmee het een stuk kan worden omgezet naar XML (zie
:ref:`xml-ref`). Dat werkt als volgt:

.. code-block:: python

    from stukkenparser import Parser, XMLAdapter

    # ...

    parser = Parser()

    stuk = parser.parse(file_name)

    xml_adapter = XMLAdapter(stuk)
    xml = xml_adapter.to_string()

    with open("xml_file.xml", "w", encoding="utf-8") as xmlf:
        xmlf.write(xml)


Voor bijv. Hoofdstuk A resulteert dit in de volgende xml.

.. code-block:: xml

    <document>
        <head>
            ...
        </head>
    <body>
        <div id="kst-35470-A-1.1" index="1" level="1" type="sectie">
            <head>Tweede Kamer der Staten-Generaal</head>
        </div>
        <div id="kst-35470-A-1.2" index="2" level="1" type="sectie">
            <head>35 470 A Jaarverslag en Slotwet Infrastructuurfonds 2019</head>
        </div>
        <div id="kst-35470-A-1.3" index="3" level="1" type="dossier">
            <head>Nr. 1 JAARVERSLAG VAN HET INFRASTRUCTUURFONDS (A)</head>
            <p id="kst-35470-A-1.3.1" index="1">Ontvangen 20 mei 2020</p>
            <div id="kst-35470-A-1.3.2" index="2" level="2" type="sectie">
                <head>1 GEREALISEERDE UITGAVEN EN ONTVANGSTEN</head>
                <img id="kst-35470-A-1.3.2.1" index="1" src="kst-35470-A-1-001.png">
                    <caption>Gerealiseerde uitgaven verdeeld over beleidsterreinen (bedragen x € 1 miljoen). Totaal € 5.760.760.000,-</caption>
                </img>
            <img id="kst-35470-A-1.3.2.2" index="2" src="kst-35470-A-1-002.png">
                <caption>Gerealiseerde ontvangsten verdeeld over beleidsterreinen (bedragen x €1 miljoen). Totaal € 5.809.341.000,-</caption>
            </img>
        </div>
        <div id="kst-35470-A-1.3.3" index="3" level="2" type="sectie">
            <head>2 ALGEMEEN</head>
                <div id="kst-35470-A-1.3.3.1" index="1" level="3" type="sectie">
                <head>2.1 Aanbieding van het jaarverslag en verzoek tot dechargeverlening</head>
                <div id="kst-35470-A-1.3.3.1.1" index="1" level="2.0" type="paragraaf">
                    <head>AAN de voorzitters van de Eerste en de Tweede Kamer van de Staten-Generaal.</head>
                    ...


Attribute callbacks
-------------------

Standaard hebben de XML-elementen de attributen `id`, `index`, `level` en
`type`. Attributen kunnen worden verwijderd of toegevoegd met behulp van
callback functies. Het toevogen van *ftext* attributen aan beleidsartikelen gaat bijvoorbeeld als volgt:

.. code-block:: python

    # ...

    def ftext_callback(ond):
        if isinstance(ond, Kop):
            if re.search(r"beleidsartikelen", ond.parent.type, re.I):
                m = re.match(
                    r"(\d(\.\d)+ )?((niet-)?beleids)?artikel (\d+)\.?", ond.text, re.I
                )
                if m:
                    return {
                        "ftext.ReferencesBudgetStructureYear": f"{ond.year}",
                        "ftext.ReferencesBudgetArticleNumber": f"{m.groups()[-1]}",
                    }

    xml_adapter = XMLAdapter(stuk, callbacks={"ftext": ftext_callback})

    xml = xml_adapter.to_string()


Output:

.. code-block:: xml

    ...
    <div id="kst-35470-A-1.3.4.2" index="2" level="3" type="beleidsartikelen">
        <head>3.2 Productartikelen</head>
        <div id="kst-35470-A-1.3.4.2.1" index="1" level="4"
            ftext.ReferencesBudgetStructureYear="2019"
            ftext.ReferencesBudgetArticleNumber="12"
            type="sectie">
            <head>3.2.1 Artikel 12 Hoofdwegennet</head>
                <div id="kst-35470-A-1.3.4.2.1.1" index="1" level="5" type="sectie">
                    <head>Omschrijving van de samenhang met het beleid</head>
            ...


.. note:: `type` is het enige attribuut dat niet verwijderd kan worden. De waarden van `type` kunnen ook worden geconfigureerd met behulp van parser-configuratie (zie :ref:`config-ref`).


Andere formats
--------------

De package bevat ook een `RDFAdapter` class (:ref:`rdf-ref`) waarmee de parse tree kan worden geconverteerd naar een
RDF-representatie zoals Turtle. 

Het schrijven van adapters voor andere formats is betrekkelijk eenvoudig. Gebruik de code van :ref:`xml-ref` en :ref:`rdf-ref` als inspiratie.
